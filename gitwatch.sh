#!/usr/bin/env bash

# Requirements
# inotify-tools
# rsync
# git (optional)

# Instructions:
# 1. Create a account at gitlab.com (https://gitlab.com/users/sign_in#register-pane).
# 2a. Create rsa SSH key without password (https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair).
# 2b. Add gitlab.com to ~/.ssh/config (https://gitlab.com/help/ssh/README#working-with-non-default-ssh-key-pair-paths).
# 2c. Go to settings and upload your public SSH key (https://gitlab.com/profile/keys).
# 3. Set the WATCH_DIR and DEST_DIR variables below.
# 4a. Create a new uninitialised private repo (https://gitlab.com/projects/new).
# 4b. In DEST_DIR, create a git repo with `git init`.
# 4c. Add the remote with `git remote add origin git@gitlab.com:...` as found in step 4a.
# 5. Run script and keep it running!

# Folder we're gonna watch recursively. No trailing slash. Absolute path.
WATCH_DIR=/tmp/itest
# Folder we're gonna copy to. No trailing slash. Absolute path.
DEST_DIR=/tmp/itest2

if [ -d "${DEST_DIR}/.git" ]; then
  cd "$DEST_DIR"
  git config push.default current
fi

# Synchronise folders, commit changes and push remotely
sync_and_commit () {
  # Synchronise files
  rsync -avhP "${WATCH_DIR}/" "$DEST_DIR"

  # CD into destination
  cd "$DEST_DIR"

  # Commit new files to git (if possible) and (try to) push to remote
  if [ -d "${DEST_DIR}/.git" ]; then
    echo "Commiting new files"
    git add .
    git commit -m "$1"

    # Push if remote configured
    if [ $(git remote) ]; then
      git push
    fi
  fi
}

# Create destination folder if necessary
mkdir -p "$DEST_DIR"

# Sync
sync_and_commit "`date`"

# Watch for created or updated files (-e close_write), indefinately (-m) and recursively (-r)
inotifywait -m -r -e close_write --format '%w%f' "$WATCH_DIR" | while read CHANGE
do
  echo "Moving file" "$CHANGE" "from" "$WATCH_DIR" "to" "$DEST_DIR"

  # Sync
  sync_and_commit "$CHANGE"
done
